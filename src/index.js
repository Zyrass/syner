// dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

// components
import { Syner, BaseStructure } from './Components'
import './index.css'

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route exact path="/" component={ Syner } />
        <Route path="/promotions" render={()=><BaseStructure name="promo"/>}/>
        <Route path="/a-propos" render={()=><BaseStructure name="about"/>}/>
        <Route path="/produits" render={()=><BaseStructure name="products"/>}/>
        <Route path="/contact" render={()=><BaseStructure name="contact"/>}/>
        <Route path="/catalogues" render={()=><BaseStructure name="catalogues"/>}/>
        <Redirect to="/" />
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);
