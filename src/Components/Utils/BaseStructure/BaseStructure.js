import React, { lazy, Suspense } from 'react'
import { Switch, Route, NavLink } from 'react-router-dom'
import Footer from '../Footer/Footer'
import NotFound from '../NotFound/NotFound'
import Logo from '../Logo/Logo'
import { BsList, BsBook } from 'react-icons/bs'
import { AiOutlineClose } from 'react-icons/ai'
import { GrDocumentPdf } from 'react-icons/gr'

import Styles from './BaseStructure.module.css'

// Component LAZY
const LazyAbout = lazy( () => import('../../Pages/About/About') );
const LazyContact = lazy( () => import('../../Pages/Contact/Contact') );
const LazyProducts = lazy( () => import('../../Pages/Products/Products') );
const LazyCatalogues = lazy( () => import('../../Pages/Products/Catalogues/CataloguesList') );
const LazyPromo = lazy( () => import('../../Pages/Products/ProductsPromo/ProductsPromo') );

class BaseStructure extends React.Component {
    constructor( props ) {
        super( props )
        this.state = {
            closeMenu: true            
        }
        console.log( {props} )
    }
    toggleMenu = (event) => {
        console.log(event)
        this.setState({
            closeMenu: !this.state.closeMenu
        })
    }
    render() {
        const { closeMenu } = this.state
        return (
            <section className={ Styles.baseStructure__container }>
                <nav className={ Styles.baseStructure__navbar}>
                    <Logo colorOne="#2f5ca1" colorTwo="#ee3f41" />
                    <span>Distribution de matériels Professionnel</span>
                    <ul>
                        <li><NavLink activeClassName="active" to="/promotions">Promotions</NavLink></li>
                        <li><NavLink to="/a-propos">A propos</NavLink></li>
                        <li><NavLink to="/produits">Produits</NavLink></li>
                        <li><NavLink to="/contact">Contact</NavLink></li>
                    </ul>

                    <button type="button" onClick={ (event) => this.toggleMenu(event) }>
                        { closeMenu === true ? <BsList /> : <AiOutlineClose /> }
                    </button>

                    {/* Ternaire pour définir si le menu est ouvert ou caché
                    "hidden" et "show" sont des display "block" et display "none" */}
                    { closeMenu === true ? null : (
                        <section className={Styles.show}>
                            <h1>Distribution de Matériels Professionnels</h1>
                            <hr/>
                            <div className={Styles.show__container}>
                                <ul>
                                    <li><NavLink activeClassName="active" to="/promotions" onClick={ () => this.toggleMenu() }>Promotions</NavLink></li>
                                    <li><NavLink to="/a-propos" onClick={ () => this.toggleMenu() }>A propos</NavLink></li>
                                    <li><NavLink to="/produits" onClick={ () => this.toggleMenu() }>Produits</NavLink></li>
                                    <li><NavLink to="/contact" onClick={ () => this.toggleMenu() }>Contact</NavLink></li>
                                </ul>
                            </div>
                            <hr/>
                            <NavLink to="/catalogues" className={Styles.btnCatalogue} ><BsBook /> Nos Catalogues</NavLink>
                            <footer>
                                <span>Icône réseau social</span>
                                <span>&copy; 2021, Syner tous droits réservé</span>
                            </footer>
                        </section>
                    )}
                </nav>
                <div>
                    <Suspense fallback={ <h1>Chargement en cours ...</h1> }>
                        <Switch>
                            <Route path="/a-propos" component={LazyAbout} />
                            <Route path="/contact" component={LazyContact} />
                            <Route path="/promotions" component={LazyPromo} />
                            <Route path="/produits" component={LazyProducts} />
                            <Route path="/catalogues" component={LazyCatalogues} />
                            <Route component={NotFound} />
                        </Switch>
                    </Suspense>
                </div>
                <aside className={Styles.aside__content}>
                    <NavLink to="/catalogues">Télécharger nos Catalogues<GrDocumentPdf /></NavLink>
                </aside>
                <footer><Footer /></footer>
            </section>
        )
    }
}

export default BaseStructure
