import React from 'react'

const LogoClient = ( {name, children} ) => {
    return (
        <figure>
            <img 
                src={children} 
                alt="" 
                title={ `Logo de la société : ${ name }` } 
                loading='lazy' 
            />
            <figcaption>{ name }</figcaption>
        </figure>
    )
}

export default LogoClient
