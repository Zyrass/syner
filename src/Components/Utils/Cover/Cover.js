import React from 'react'
import Styles from './Cover.module.css'

const Cover = ({ title }) => {
    return (
        <header className={ Styles.cover__content }>
            <h1 data-title={ title }>{title}</h1>
        </header>
    )
}

export default Cover
