// dépendances
import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'

const Logo = ( {colorOne, colorTwo} ) => {
    return (
        <Fragment>
            <Link to="/">
                <figure title="Syner spécialiste dans la Distribution de Matériels Professionnels">
                    {/* <?xml version="1.0" standalone="no"?>
                    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN"
                    "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd"> */}
                    <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
                    width="322.000000pt" height="143.000000pt" viewBox="0 0 322.000000 143.000000"
                    preserveAspectRatio="xMidYMid meet">
                        <metadata>
                            Created by potrace 1.16, written by Peter Selinger 2001-2019
                        </metadata>

                        {/* Trait Syner Bleu */}
                        <g transform="translate(0.000000,143.000000) scale(0.100000,-0.100000)"
                    fill={colorOne} stroke="none">
                            <path d="M419 1215 c-125 -40 -188 -192 -144 -342 20 -66 83 -137 201 -224 50
                    -37 102 -81 114 -99 48 -68 46 -151 -5 -206 -28 -29 -39 -34 -80 -34 -26 0
                    -60 7 -76 17 -28 17 -59 75 -59 113 0 18 -7 20 -64 20 l-64 0 5 -43 c15 -117
                    86 -213 175 -237 57 -16 144 -7 196 19 86 44 140 161 128 280 -11 124 -59 189
                    -227 306 -87 61 -114 91 -131 146 -33 110 73 198 169 139 23 -14 35 -31 44
                    -62 6 -24 13 -49 15 -56 3 -9 21 -12 61 -10 l58 3 -3 40 c-3 49 -28 121 -56
                    158 -51 70 -165 102 -257 72z"/>
                            <path d="M800 1183 c5 -16 52 -167 105 -338 l95 -309 0 -173 0 -173 60 0 60 0
                    0 160 0 161 105 340 c58 188 105 345 105 350 0 5 -27 9 -60 9 l-59 0 -19 -62
                    c-11 -35 -43 -148 -72 -251 -29 -103 -56 -184 -60 -180 -3 4 -37 117 -74 251
                    l-69 242 -63 0 -63 0 9 -27z"/>
                            <path d="M1430 700 l0 -510 61 0 62 0 -5 370 c-2 204 -3 370 -2 370 1 0 17
                    -44 34 -97 24 -74 164 -445 237 -630 3 -9 26 -13 64 -13 l59 0 0 510 0 510
                    -56 0 -56 0 3 -370 c2 -203 1 -366 -2 -362 -4 4 -12 25 -19 47 -15 48 -175
                    472 -249 658 -10 25 -14 27 -71 27 l-60 0 0 -510z"/>
                            <path d="M2130 1141 l0 -71 283 0 c303 0 313 -2 351 -53 17 -23 22 -46 24
                    -120 5 -141 -30 -190 -144 -204 l-61 -8 115 -247 115 -248 64 0 c34 0 63 3 63
                    6 0 3 -41 89 -90 192 -50 102 -87 188 -83 192 5 3 25 18 45 33 123 90 138 401
                    24 525 -11 13 -39 34 -61 45 -38 21 -55 22 -342 25 l-303 3 0 -70z"/>
                        </g>

                        {/* Trait R-milieu Rouge */}
                        <g transform="translate(0.000000,143.000000) scale(0.100000,-0.100000)"
                                fill={colorTwo} stroke="none"
                        >
                            <path d="M2130 695 l0 -65 140 0 140 0 0 65 0 65 -140 0 -140 0 0 -65z"/>
                        </g>

                        {/* Trait R-bas Bleu */}
                        <g transform="translate(0.000000,143.000000) scale(0.100000,-0.100000)"
                            fill={colorOne} 
                            stroke="none"
                        >
                            <path d="M2130 255 l0 -65 215 0 215 0 0 65 0 65 -215 0 -215 0 0 -65z"/>
                        </g>
                    </svg>
                </figure>
            </Link>
        </Fragment>
    )
}

export default Logo
