import Cover from "../Cover/Cover"

const NotFound = () => {
    return (
        <>
            <Cover title="Error 404" />
            <img src="./images/404.jpg" 
                alt="illustration d'une page introuvable" 
                loading="lazy" 
                style={{
                    margin: "auto",
                    width: "80%",
                }}/>
        </>
    )
}

export default NotFound