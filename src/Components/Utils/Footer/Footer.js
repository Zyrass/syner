import React from 'react'
import Styles from './Footer.module.css'

const Footer = () => {
    return (
        <>
            <div className={Styles.footer__content}>
                
                <div className={Styles.footer__blockOne}>
                    <div>
                        <h4>Horaires</h4>
                        <table>
                            <thead>
                                <tr>
                                    <th>Jour</th>
                                    <th>Horaires Matin</th>
                                    <th>Horaires Après-Midi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Lundi</td>
                                    <td>7h - 12h</td>
                                    <td>13h30 - 17h</td>
                                </tr>
                                <tr>
                                    <td>Mardi</td>
                                    <td>7h - 12h</td>
                                    <td>13h30 - 17h</td>
                                </tr>
                                <tr>
                                    <td>Mercredi</td>
                                    <td>7h - 12h</td>
                                    <td>13h30 - 17h</td>
                                </tr>
                                <tr>
                                    <td>Jeudi</td>
                                    <td>7h - 12h</td>
                                    <td>13h30 - 17h</td>
                                </tr>
                                <tr>
                                    <td>Vendredi</td>
                                    <td>7h - 12h</td>
                                    <td>13h30 - 17h</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>{/* end horaires */}

                    <div>
                        <h4>Magasin</h4>
                        <address>
                            50 rue Jean Zay<br />
                            69800 Saint-Priest<br />
                            <span>04 82 29 96 87</span>
                        </address>
                    </div>{/* end magasin */}

                </div>{/* end block-one */}

                <div className={Styles.footer__blockTwo}>
                    <span>&copy; Copyright 2021 - Syner - Tous droits réservé</span>
                </div>{/* end block-two */}

            </div>{/* end footer__content */}

        </>
    )
}

export default Footer