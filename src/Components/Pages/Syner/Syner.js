// dépendances
import Typewriter from 'typewriter-effect'
import { NavLink } from 'react-router-dom'

// components + scss
import Logo from "../../Utils/Logo/Logo"

import Styles from './Syner.module.css'

const Syner = () => {
  return (
    <section className={Styles.syner__container}>
      <Logo colorOne="#2f5ca1" colorTwo="#ee3f41" />
      <p>   
        <Typewriter
          options={{
            strings: ['Distribution de matériels Professionnel'],
            autoStart: true,
            loop: true,
          }}
        />
      </p>

      <button><NavLink to="/a-propos">Entrée</NavLink></button>
    </section>
  )
}

export default Syner
