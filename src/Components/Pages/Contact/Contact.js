import React from 'react'
import { Formik, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup'

import Cover from '../../Utils/Cover/Cover';
import Styles from './Contact.module.css';

const InputComponent = ({
    field,
    meta,
    form: { touched, errors },
    ...props
}) => (
    <div className="form-group">
        <label htmlFor={props.name}>{ props.label }</label>
        <input { ...props } { ...field } className="form-control" />
        { 
            touched[field.name] &&
            errors[field.name] && 
            <span style={{ color: "red"}}><small>{ errors[field.name] }</small></span>
        }
    </div>
)

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
class Contact extends React.Component {

    // .shape() décris chacun des éléments de l'objet.
    userSchema = Yup.object().shape({
        lastname: Yup.string().min(3, 'trop court').max(25, 'trop long').required('Champ requis'),
        firstname: Yup.string().min(3, 'trop court').max(25, 'trop long').required('Champ requis'),
        society: Yup.string().min(3, 'trop court').max(25, 'trop long').required('Champ requis'),
        email: Yup.string().email('Mail incorrect').required('Champ requis'),
        tel: Yup.string().matches(phoneRegExp, 'Téléphone invalide').required('Champ requis'),
        message: Yup.string().min(15, '15 caractères minimum requis...').max(500, "500 caractères maximum").required('Champ requis')
    })

    submit = (values, actions) => {
        console.log( values )
        console.log( actions )
        // actions.setSubmitting(false) // Permet de rés-activer le bouton d'envoie.
        // actions.resetForm()
    }

    render() {
        return (
            <>
                <Cover title="Contact" image="./images/fourniture.jpg" />
                <div className={ Styles.contact__content }>
                    <p>
                        N'hésitez surtout pas à prendre contact avec nous, 
                        nous vous répondrons dans les plus bref délais.
                    </p>
                    <div>
                        <Formik
                            onSubmit={ this.submit }
                            initialValues={{
                                civility: '',
                                lastname: '',
                                firstname: '',
                                society: '',
                                email: '',
                                tel: '',
                                object: '',
                                message: ''
                            }}
                            // validate= { this.validate } // remplacé par YUP
                            validationSchema={ this.userSchema }
                            validateOnBlur= { true }
                            validateOnChange= { true }
                        >
                            { ({
                                handleSubmit,
                                values,
                                isSubmitting,
                                errors,
                                touched
                            }) => (
                                <form onSubmit={handleSubmit}>

                                    <Field as="select" name="civility">
                                        <option>Civilité ?</option>
                                        <option value="mme">Madame</option>
                                        <option value="mr">Monsieur</option>
                                    </Field>
                                    
                                    <Field 
                                        name="lastname" 
                                        type="text" 
                                        placeholder="Votre nom de famille" 
                                        label="Nom" 
                                        Component={ InputComponent } 
                                    />
                                    <ErrorMessage name="lastname" >
                                        { errorMessage => <small>{errorMessage}</small>}
                                    </ErrorMessage>


                                    <Field 
                                        name="firstname"
                                        type="text" 
                                        placeholder="Votre prénom" 
                                        label="Prénom" 
                                        Component={ InputComponent } 
                                    />
                                    <ErrorMessage name="firstname" >
                                        { errorMessage => <small>{errorMessage}</small>}
                                    </ErrorMessage>
                                    
                                    <Field 
                                        name="society" 
                                        type="text" 
                                        placeholder="Le nom de votre société" 
                                        label="Société" 
                                        Component={ InputComponent } 
                                    />
                                    <ErrorMessage name="society" >
                                        { errorMessage => <small>{errorMessage}</small>}
                                    </ErrorMessage>

                                    <Field 
                                        name="email" 
                                        type="email" 
                                        placeholder="Votre mail" label="E-mail" 
                                        Component={ InputComponent } 
                                    />
                                    <ErrorMessage name="email" >
                                        { errorMessage => <small>{errorMessage}</small>}
                                    </ErrorMessage>

                                    <Field 
                                        name="tel" 
                                        type="tel" 
                                        placeholder="Un téléphone" 
                                        label="Téléphone" 
                                        Component={ InputComponent } 
                                    />
                                    <ErrorMessage name="tel" >
                                        { errorMessage => <small>{errorMessage}</small>}
                                    </ErrorMessage>

                                    <Field 
                                        as="select" 
                                        name="object"
                                    >
                                        <option>Sélectionné l'objet de votre message ?</option>
                                        <option value="commande">J'ai une commande à passer</option>
                                        <option value="rendezVous">Prendre un rendez-vous</option>
                                        <option value="reclamation">Faire une réclamation</option>
                                        <option value="bug">Signaler une erreur sur le site</option>
                                        <option value="autre">Autre demande</option>
                                    </Field>

                                    <Field 
                                        as="textarea" 
                                        name="message" 
                                        cols="30" 
                                        rows="10" 
                                        label="message" 
                                    />
                                    <ErrorMessage name="message" >
                                        { errorMessage => <small>{errorMessage}</small>}
                                    </ErrorMessage>

                                    <button disabled={isSubmitting} type="submit">Envoyer</button>
                                </form>
                            ) }
                        </Formik>
                        <iframe 
                            style={{ border:"none"}} 
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6626.251920551864!2d4.904070793758342!3d45.71112223046332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f4c3cd4b51c963%3A0xb7e26017a0e3a73f!2s50%20Rue%20Jean%20Zay%2C%2069800%20Saint-Priest!5e0!3m2!1sfr!2sfr!4v1615220605992!5m2!1sfr!2sfr" 
                            width="100%" 
                            height="500px" 
                            allowfullscreen="" 
                            loading="lazy"
                            title="Représentation via une carte de google de notre localisation.">
                        </iframe>
                    </div>
                </div>
            </>
        )
    }
}

export default Contact