import React from 'react'
import Cover from '../../Utils/Cover/Cover';
import LogoClient from '../../Utils/LogoClient/LogoClient'

import Styles from "./About.module.css";

const About = (props) => {
    return (
        <>
            <Cover title="A propos" />
            <div className={Styles.about__content}>
                <div>
                    <h2>Présentation</h2>
                    <div>
                        <p>
                            Nous sommes une entreprise spécialisée dans le commerce de gros en tant que quincailler.<br />
                            <strong>Nos qualités sont nombreuses, nous sommes jeunes, ambitieux,
                            téméraires, déterminés</strong>. 
                        </p>
                        <figure>
                            <img src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80" alt="Mr Umberto SIMONETTI" loading="lazy" title="Président - Mr Umberto SIMONETTI"/>
                            <img src="https://images.unsplash.com/photo-1500832333538-837287aad2b6?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80" alt="Mr Jean-Michel WNEK" loading="lazy" title="Directeur général - Mr Jean-Michel WNEK" />
                            <figcaption>
                                à gauche, Umberto SIMONETTI<br />
                                à droite, Jean-Michel WNEK
                            </figcaption>
                        </figure>
                    </div>
                </div>

                <div>
                    <h2>Historique</h2>
                    <p>
                        Historiquement nous avons créer la société en mai 2019 et aujourd'hui <strong>grâce à notre écoute, un service adapté à chacun et à la qualité de notre marchandise, un grand nombre de clients nous font confiance</strong> et aide notre société s'accroître exponentiellement. En liaison constante avec notre clientèle, nous nous améliorons au quotidien afin de vous satisfaire.
                    </p>
                    <img src="https://img.e-marketing.fr/Img/BREVE/2019/9/341402/Comment-reconquerir-confiance--LE.jpg" alt=""/>
                </div>
                
                <div>
                    <h2>Quelques statistiques</h2>
                    <div data-stats="+1000"><span>Clients</span></div>
                    <div data-stats="52"><span>Fournisseurs</span></div>
                    <div data-stats="5"><span>Personnels</span></div>
                    <div data-stats="1"><span>Magasins</span></div>
                </div>
                
                <div>
                    <h2>Ils nous font confiance</h2>
                    <div>
                        <LogoClient name="INEO">https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/ENGIE_ineo_logo.jpg/1200px-ENGIE_ineo_logo.jpg</LogoClient>
                        <LogoClient name="INEO">https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/ENGIE_ineo_logo.jpg/1200px-ENGIE_ineo_logo.jpg</LogoClient>
                        <LogoClient name="INEO">https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/ENGIE_ineo_logo.jpg/1200px-ENGIE_ineo_logo.jpg</LogoClient>
                        <LogoClient name="INEO">https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/ENGIE_ineo_logo.jpg/1200px-ENGIE_ineo_logo.jpg</LogoClient>
                        <LogoClient name="INEO">https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/ENGIE_ineo_logo.jpg/1200px-ENGIE_ineo_logo.jpg</LogoClient>
                        <LogoClient name="INEO">https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/ENGIE_ineo_logo.jpg/1200px-ENGIE_ineo_logo.jpg</LogoClient>
                    </div>
                </div>

                <div>
                    <h2>Exemple</h2>
                    <p>
                        <em>Par exemple, nos conditions de stockage sont souples et peuvent être aménagées pour satisfaire chaque client.</em>
                    </p>
                </div>
            </div>
        </>
    )
}

export default About
