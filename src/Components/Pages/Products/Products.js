import React from 'react'
import Cover from '../../Utils/Cover/Cover';

class Products extends React.Component {
    render() {
        return (
            <>
                <Cover title="Produits" />
                <h2 style={{padding: "20px"}}>à éditer</h2>
                <p style={{padding: "25px"}}>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio maiores sapiente at accusamus, inventore, aspernatur voluptatem molestias qui et odio doloremque maxime itaque animi sit! Illo itaque dignissimos fuga quam vel ipsa cumque nesciunt, optio quas facere, facilis explicabo mollitia quae perspiciatis architecto reprehenderit quia iusto maiores voluptatibus modi. Incidunt distinctio id necessitatibus numquam labore! Nesciunt est aut facilis facere sunt quam aspernatur harum provident magnam. Tenetur culpa iste aperiam magnam iusto doloribus ea rem nesciunt error accusantium deleniti ducimus, commodi iure consectetur laboriosam molestiae animi. Sunt, quia voluptatibus veniam enim dolorem sequi similique temporibus inventore pariatur nisi reiciendis repellat commodi quibusdam numquam. Repellat at maxime architecto distinctio voluptas, ea laboriosam officia voluptates, dolores officiis aut perspiciatis numquam optio, odio ratione inventore animi nostrum! Nisi voluptates beatae ipsa aliquid porro maxime eius similique nihil voluptas perspiciatis eum voluptatibus velit quod, nesciunt pariatur adipisci. Expedita, laborum. Non veritatis optio illo neque ratione praesentium dolorum nihil exercitationem rerum maxime eum, veniam hic obcaecati a possimus velit dolores molestiae ex voluptatum, vero aspernatur minus corporis tenetur? Deserunt deleniti possimus totam modi debitis. Quos molestias aperiam eaque soluta officiis. Nihil dicta illo iure possimus asperiores placeat sequi voluptas laudantium velit earum enim, vero provident ut. Rerum voluptas non in officiis voluptatum, architecto a tenetur perspiciatis accusantium corporis autem quod dolores sapiente cumque omnis maxime voluptatibus itaque! Quos reiciendis voluptas repellat eum quisquam necessitatibus nam, dicta nulla! Reprehenderit harum nihil possimus cumque expedita autem est voluptatum totam sint similique qui eaque fugit recusandae, culpa, beatae quis repudiandae repellat. Ducimus consectetur dicta maxime eius, qui ab magni, ea, neque dignissimos totam pariatur amet. Eaque optio possimus saepe nisi, eligendi est voluptatum nesciunt modi tempore quas nobis, reiciendis quisquam suscipit. Earum doloremque quia, molestias, vel minus magnam tenetur corporis laborum facilis temporibus exercitationem, at cumque? Sit, officiis.
                </p>
            </>
        )
    }
}

export default Products
