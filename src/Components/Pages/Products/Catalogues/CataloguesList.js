import React from 'react'
import Cover from '../../../Utils/Cover/Cover';
import Catalogue from "./Catalogue"

import Styles from './CataloguesList.module.css';

class CataloguesList extends React.Component {
    render() {

        return (
            <>
                <Cover title="Catalogues" />
                <section className={ Styles.catalogues__content }>
                    <p>
                        Syner vous propose ici de télécharger directement tous les catalogues provenant de chacun de nos fournisseurs.
                    </p>
                    <div>
                        <Catalogue marque="Makita" title="Tract 01 2021" download="https://makita-groupe.fr/makita/brochures/catalogues/2021/Tract_01_2021_VSP_BD.pdf">
                            https://www.makita.fr/brochures/catalogues/2021/Vignette_Tract_01_2021.jpg
                        </Catalogue>
                        <Catalogue marque="Makita" title="Tract 01 2021" download="https://makita-groupe.fr/makita/brochures/catalogues/2021/Tract_01_2021_VSP_BD.pdf">
                            https://www.makita.fr/brochures/catalogues/2021/Vignette_Tract_01_2021.jpg
                        </Catalogue>
                        <Catalogue marque="Makita" title="Tract 01 2021" download="https://makita-groupe.fr/makita/brochures/catalogues/2021/Tract_01_2021_VSP_BD.pdf">
                            https://www.makita.fr/brochures/catalogues/2021/Vignette_Tract_01_2021.jpg
                        </Catalogue>
                        <Catalogue marque="Makita" title="Tract 01 2021" download="https://makita-groupe.fr/makita/brochures/catalogues/2021/Tract_01_2021_VSP_BD.pdf">
                            https://www.makita.fr/brochures/catalogues/2021/Vignette_Tract_01_2021.jpg
                        </Catalogue>
                        <Catalogue marque="Makita" title="Tract 01 2021" download="https://makita-groupe.fr/makita/brochures/catalogues/2021/Tract_01_2021_VSP_BD.pdf">
                            https://www.makita.fr/brochures/catalogues/2021/Vignette_Tract_01_2021.jpg
                        </Catalogue>
                        <Catalogue marque="Makita" title="Tract 01 2021" download="https://makita-groupe.fr/makita/brochures/catalogues/2021/Tract_01_2021_VSP_BD.pdf">
                            https://www.makita.fr/brochures/catalogues/2021/Vignette_Tract_01_2021.jpg
                        </Catalogue>
                        <Catalogue marque="Makita" title="Tract 01 2021" download="https://makita-groupe.fr/makita/brochures/catalogues/2021/Tract_01_2021_VSP_BD.pdf">
                            https://www.makita.fr/brochures/catalogues/2021/Vignette_Tract_01_2021.jpg
                        </Catalogue>
                    </div>
                </section>
            </>
        )
    }
}

export default CataloguesList
