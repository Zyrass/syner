import React from 'react'
import { Link } from 'react-router-dom'

import Styles from './Catalogue.module.css'

const Catalogue = ({ marque, download, title, children }) => {
    return (
        <>
            <div className={Styles.card__content}>
                <h3>{marque}</h3>
                <figure>
                    <img src={children} alt={title} width="100" />
                    <figcaption>{ title }</figcaption>
                </figure>
                <div>
                    <Link to="" download={download}>icone DL</Link>
                </div>
            </div>
        </>
    )
}

export default Catalogue
