export { default as Syner } from './Pages/Syner/Syner';

export { default as BaseStructure } from './Utils/BaseStructure/BaseStructure';
export { default as NotFound } from './Utils/NotFound/NotFound';